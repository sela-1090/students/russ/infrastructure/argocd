<<<<<<< HEAD
# ArgoCD installation
![ez logo](/resources/images/ez/ez-smiley-small-logo.png)

## install ArgoCD instance on Kubernetes
(based on this page: https://argo-cd.readthedocs.io/en/stable/getting_started/)

### Requirements
- Installed kubectl command-line tool.
- Connected to a Kubernetes cluster - Have a kubeconfig file (default location is ~/.kube/config).
 
### Install Argo CD
```
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```
This will create a new namespace, argocd, where Argo CD services and application resources will live.

Change the argocd-server service type to LoadBalancer:
```
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'
```

Get the et argocd-initial-admin-secret -o jsonpath='{.data.password}' | base64 -d
```

You can now access the Argo CD UI from your browser by typing the following URL:
```
https://34.88.94.81/login?return_url=https%3A%2F%2F34.88.94.81%2Fapplications
```
where the IP is the external-service-IP of the 'argocd-server' service

Login to Argo CD UI using the username: 'admin' and and the above passwordpassword from the new terminal using this command:
```
kubectl -n argocd get secr

#Deployments
![Deployments](deployments.jpg)

